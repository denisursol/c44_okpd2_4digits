from clickhouse_driver import connect
import pymongo
from datetime import datetime
import pandas as pd
from bson.tz_util import FixedOffset
import os
from dotenv import load_dotenv
import re
from pandas import DataFrame

debugStatus = False

def flushBuffer():
    print(' flushing buffer... ', end='')
    global needToBeFlushed
    df = pd.DataFrame(data)
    #    with pd.option_context('display.max_rows', None, 'display.max_columns',
    #                           None):  # more options can be specified also
    #        print(df)
    #    #print(df)

    Query = "INSERT INTO " + c44Okpd2TableName + " VALUES "

    if not debugStatus:
        cursor.execute(Query, df.to_dict('records'))

    needToBeFlushed = False
    print('OK')
    del data[:]


def is_ok(text):
    match = re.match("""^[а-яА-ЯёЁ][а-яё0-9 !?:;"'.,]+$""", text)
    return bool(match)

print('c44_okpd2_4digits', end='')
if debugStatus:
    print(' DEBUG MODE ON!!!!')
else:
    print()

host = os.environ.get('OS')
if host is None:
    host = 'Linux'
load_dotenv()
contracts44_user = os.environ.get('contracts44_user')
contracts44_password = os.environ.get('contracts44_password')
readall_user = os.environ.get('readall_user')
readall_password = os.environ.get('readall_password')

if 'Windows' in host:
    mongoHosts = ','.join(['mongoSber1.multitender.ru:8635', 'mongoSber2.multitender.ru:8635'])
    clickHost = '217.25.154.134'
else:
#    mongoHosts = ','.join(['192.168.0.140:8635', '192.168.0.202:8635'])
    mongoHosts = ','.join(['mongoSber1.multitender.ru:8635', 'mongoSber2.multitender.ru:8635'])
    clickHost = '172.18.3.162'


print('Connecting to clickhouse ', clickHost, '... ', end='')


conn = connect(host=clickHost, user="default", password="", port=9000)
cursor = conn.cursor()
print('OK!')
print('Connecting to MongoDB', mongoHosts, '... ', end = '')

urlMongo = 'mongodb://{user}:{pw}@{hosts}/?authSource={auth_src}'.format(
    user = readall_user,
    pw = readall_password,
    hosts = mongoHosts,
    auth_src = 'admin')

client = pymongo.MongoClient(
    urlMongo,
    tls=True,
    authMechanism="SCRAM-SHA-1",
    tlsAllowInvalidHostnames=True,
    tlsCAFile='/temp/sber.crt')

database = client["nsi44"]
collection = database["nsiOKPD2"]
query = {}
query["$and"] = [
    {
        u"actual": True
    },
    {
        u"$expr" : { "$eq" : [ { "$strLenCP" : "$code"}, 5 ]}
    }
]

codeToName = {}
dbcursor = collection.find(query)
try:
    for doc in dbcursor:
        name = doc['name']
        if is_ok(name):
            codeToName[doc['code']] = doc['name']
            print(doc)
finally:
    client.close()

urlMongo = 'mongodb://{user}:{pw}@{hosts}/?authSource={auth_src}'.format(
    user = contracts44_user,     #quote('contracts44'),
    pw = contracts44_password,       #quote('fGauA73Pf4XaTVFmdwqxzSL67Q1PvKJo'),

    #hosts=','.join(['mongoSber1.multitender.ru:8635', 'mongoSber2.multitender.ru:8635']),
    hosts = mongoHosts,
    auth_src = 'contracts44')

client1 = pymongo.MongoClient(
    urlMongo,
    tls=True,
    authMechanism="SCRAM-SHA-1",
    tlsAllowInvalidHostnames=True,
    tlsCAFile='/temp/sber.crt')

db1 = client1['contracts44']
collection1 = db1['contract']

print('. Connection OK!')

tbPrefix = "temp_c44_okpd2_4digits"
# unixTimeStamp = int(time.time())
# c615TableName = tbPrefix+str(unixTimeStamp)
c44Okpd2TableName = tbPrefix

badTables = []
cursor.execute('SHOW TABLES;')
for tb in cursor.fetchall():
    #print(tb[0])
    if tbPrefix in tb[0]:
        Query = tb[0]
        Query = "DROP TABLE " + Query + ";"
        badTables.append([Query])

for Query in badTables:
    pass
    print("DELETE -", Query[0])
    if not debugStatus:
        pass
        cursor.execute(Query[0])

Query = ''.join(["CREATE TABLE ", c44Okpd2TableName])
Query = Query + " (regNum String, okpd2 String, name String) ENGINE MergeTree() ORDER BY(okpd2)"
# print('Query = ',Query)
try:
    if not debugStatus: cursor.execute(Query)
except:
    print('Error while try to create table ', c44Okpd2TableName, '!')

globalMaxCount = 0
max_count = 0
data = []

query = {}
query["protocolDate"] = {
    u"$gte": datetime.strptime("2018-01-01 00:00:00.000000", "%Y-%m-%d %H:%M:%S.%f").replace(tzinfo = FixedOffset(180, "+0300")),
    u"$lte": datetime.strptime("2022-12-31 23:59:59.000000", "%Y-%m-%d %H:%M:%S.%f").replace(tzinfo = FixedOffset(180, "+0300"))
}
projection = {}
projection["regNum"] = 1.0
projection["products"] = 1.0


needToBeFlushed = False

print('Collecting data...')
for doc in collection1.find(query, projection = projection):
    if debugStatus:
        print(doc)
    okpd2arr = []
    max_count += 1
    globalMaxCount += 1
    r_id = str(doc['_id'])

    try:
        r_regNum = str(doc['regNum'])
        r_maxId = r_regNum
        needToBeFlushed = True
    except:
        continue

    productsAr = []
    try:
        tempVar = len(doc['products']['product'])
    except:
        if debugStatus: print('NO len(doc[products][product])')
        continue

    try:
        if len(doc['products']['product']['OKPD2']) > 0:
            if debugStatus:  print('doc[products][product][ONE]-', doc['products']['product'])
            productsAr.append({'OKPD2': doc['products']['product']['OKPD2']})
    except:
        pass
    try:
        if  len(doc['products']['product']['KTRU']) > 0:
            if debugStatus:  print('doc[products][product][ONE]-', doc['products']['product'])
            productsAr.append({'KTRU': doc['products']['product']['KTRU']})
    except:
        pass
#    try:
#        if len(doc['products']['product']['OKPD']) > 0:
#            if debugStatus:  print('doc[products][product][ONE]-', doc['products']['product'])
#            productsAr.append({'OKPD': doc['products']['product']['OKPD']})
#    except:
#        pass

    if debugStatus:
        print('productsAr=', productsAr)

    if len(productsAr)<1:
        if debugStatus:
            print('in doc[products][product][MANY] -', doc)
            print('doc[products][product]', doc['products']['product'])
        productsAr = doc['products']['product']

    for product in productsAr:
        if debugStatus:
            print(product)

        pass
        try:
            okpd2 = product['OKPD2']['code']
            okpd2arr.append(okpd2[0:5])
        except:
            pass
        try:
            ktru = product['KTRU']['code']
            okpd2arr.append(ktru[0:5])
        except:
            pass
        #try:
        #    okpd = product['OKPD']['code']
        #    okpd2arr.append(okpd[0:5])
        #except:
        #    pass
    if debugStatus:
        print(okpd2arr)
    okpd2arr = list(set(okpd2arr))

    for okpd2 in okpd2arr:

        if okpd2 in codeToName:
            name = codeToName[okpd2]
        else:
            name = ''
        new_row = {'regNum': r_regNum,
                   'okpd2': okpd2,
                   'name': name}
        data.append(new_row)
        if debugStatus:
            print(new_row)
    if debugStatus:
        print(data)
    print("\r", globalMaxCount, end='')
    if max_count == 100000:
        max_count = 0
        flushBuffer()

if needToBeFlushed:
    flushBuffer()

Query = "DROP TABLE c44_okpd2_4digits"
try:
    if not debugStatus:
        cursor.execute(Query)
        print('DROP OLD TABLE SUCCESS')
except:
    print('Can not DROP TABLE c44_okpd2_4digits')
        

Query = "rename table temp_c44_okpd2_4digits to c44_okpd2_4digits"
if not debugStatus:
    try:
        cursor.execute(Query)
        print('RENAME NEW TABLE TO OLD WAS SUCCESS')
    except:
        print('Can\'t rename temp_c44_okpd2_4digits to c44_okpd2_4digits')
        exit(1)


tbPrefix = "c44_ai_"  # 1678438199"
cursor.execute('SHOW TABLES;')
for tb in cursor.fetchall():
    if (tbPrefix in tb[0]):
        c44_ai_tableName = tb[0]


#debugStatus = False
Query = "drop view c44_overall_data_4digits"
if not debugStatus:
    try:
        cursor.execute(Query)
    except:
        print("can't delete view", Query)
#
Query = "CREATE MATERIALIZED VIEW c44_overall_data_4digits ENGINE = AggregatingMergeTree() \
    ORDER BY (supplierInn, regNum) populate as select price, regNum, supplierInn, executionPeriodEndDate, \
    contractStage, c44_okpd2_4digits.okpd2 as okpd, c44_okpd2_4digits.name as name from " + c44_ai_tableName + \
    " inner join c44_okpd2_4digits on " + c44_ai_tableName + ".regNum= c44_okpd2_4digits.regNum \
    where contractStage=\'EC\'"

if not debugStatus:
    try:
        cursor.execute(Query)
    except:
        print("can't create view", Query)
        exit(1)


Query = "select okpd, name, sum(price) as summ_each_okpd, count(price) as dog_count from c44_overall_data_4digits group by okpd,name order by okpd"
if not debugStatus:
    try:
        cursor.execute(Query)
    except:
        print("can't execute select with group from view", Query)
        exit(1)

code = []
name = []
summ = []
count = []
for tb in cursor.fetchall():
    #print(tb)
    code.append(tb[0])
    name.append(tb[1])
    summ.append(tb[2])
    count.append(tb[3])
df = DataFrame({'Код': code, 'Расшифровка': name, 'Сумма контрактов': summ, 'Количество контрактов': count})
df.to_excel('zakaz.xlsx', sheet_name='sheet1', index=False)




print('Closing connections.')
conn.close()

client1.close()

print('Finished.')
